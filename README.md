
Recast & Detour
===============

Based on https://github.com/jackpoz/recastnavigation (original project: https://github.com/recastnavigation/recastnavigation).

See [docs](https://gitlab.com/opfesoft/sol-docs/-/blob/master/misc/View-Maps.md) on how to use RecastDemo to view maps / vmaps.

## License

Recast & Detour is licensed under ZLib license, see License.txt for more information.
